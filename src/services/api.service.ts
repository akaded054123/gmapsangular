import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
//import { Student } from '../models/student';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

constructor(private http: HttpClient) { }

  // API path
  base_path = 'http://localhost:3000/api/';


  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error.lat}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
///////////  BUS  /////////////
  getBus(): Observable<any> {
    return this.http
      .get<any>(this.base_path+'bus/')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  getBusById(id): Observable<any> {
    return this.http
      .get<any>(this.base_path+'bus/'+id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  addBus(data): Observable<any> {
    console.log(data);
    return this.http
      .post<any>(this.base_path+'bus/', data, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  updateBus(id,data): Observable<any> {
    console.log(id,data);
    return this.http
      .put<any>(this.base_path+'bus/'+id, data, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
  deleteBus(id) {
    return this.http
      .delete<any>(this.base_path+'bus/' + id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }


  ///////////////////////


  ///////////  BUS INFO  //////////////////

  getBus_info(): Observable<any> {
  return this.http
    .get<any>(this.base_path+'bus_info/')
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }

  getBus_infoById(id): Observable<any> {
  return this.http
    .get<any>(this.base_path+'bus_info/'+id)
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }

  addBus_info(data): Observable<any> {
  console.log(data);
  return this.http
    .post<any>(this.base_path+'bus_info/', data, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }

  updateBus_info(id,data): Observable<any> {
  console.log(id,data);
  return this.http
    .put<any>(this.base_path+'bus_info/'+id, data, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }
  deleteBus_info(id) {
  return this.http
    .delete<any>(this.base_path+'bus_info/'+id, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }

  //////////////////////////////////////////////////

  ////////////////  BUS STOP  //////////////////////

  getBus_stop(): Observable<any> {
  return this.http
    .get<any>(this.base_path+'bus_stop/')
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }

  getBus_stopById(id): Observable<any> {
  return this.http
    .get<any>(this.base_path+'bus_stop/'+id)
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }

  addBus_stop(data): Observable<any> {
  console.log(data);
  return this.http
    .post<any>(this.base_path+'bus_stop/', data, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }

  updateBus_stop(id,data): Observable<any> {
  console.log(id,data);
  return this.http
    .put<any>(this.base_path+'bus_stop/'+id, data, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }
  deleteBus_stop(id) {
  return this.http
    .delete<any>(this.base_path+'bus_stop/'+id, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handleError)
    )
  }

/////////////////////////////////////

  getWaypoint(): Observable<any> {
    return this.http
      .get<any>(this.base_path+'func/wp')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
  getRedPath(): Observable<any> {
    return this.http
      .get<any>(this.base_path+'path/red')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
  getYellowPath(): Observable<any> {
    return this.http
      .get<any>(this.base_path+'path/yellow')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
  // Create a new item
  findNearest(data): Observable<any> {
    console.log(data);
    return this.http
      .post<any>(this.base_path+'func/findNearest', data, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get single student data by ID
  getItem(id): Observable<any> {
    return this.http
      .get<any>(this.base_path + '/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get students data


  // Update item by id
  updateItem(id, item): Observable<any> {
    return this.http
      .put<any>(this.base_path + '/' + id, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Delete item by id
  deleteItem(id) {
    return this.http
      .delete<any>(this.base_path + '/' + id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

}
