import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/services/api.service';
import { Interface_busstop } from './interface_busstop';
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { AgmInfoWindow } from '@agm/core/directives/info-window';
@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit {
  shiw:Interface_busstop[];
  info_data:Interface_busstop[];
  lat:any;
  lng:any;
  name:any;;
  data: any;
  location: Location;
  iconbus: Location;
  wp: any;
  flow_data: any;
  number_busstop: number;
  number_bus: number = 0;
  stream_datas= [];
  markers = [];
  bus_marker = [];
  waypoint_marker = [];
  callwaypoint: boolean = false;
  constructor(public apiService: ApiService, private socket: Socket) { 
    this.data = [];
  }
  

  ngOnInit() {
    this.receiveData();
    this.infoBusStop();
    this.callBusStop();
    this.apiService.getWaypoint().subscribe(res1 => {
      this.wp = res1;
    })
    
    this.location = {
      latitude: 16.744822,
      longitude: 100.194718,
      zoom: 15.8,
      scrollwheel:  false,
      iconUrl: './assets/rsz_bus-stop.png' ,
      disableDoubleClickZoom: true,
      markers: [],
      bus_marker :[],
      waypoint_marker:[],
      name: [],
      path: ""
      
  } 
}

  callBusStop(){
  this.apiService.getBus_stop().subscribe((busstop:Interface_busstop[]) =>{
    this.shiw = busstop['data'];
    this.number_busstop = busstop.length;
    for(let i=0; i< busstop.length;i++){
      this.location.markers.push({
        lat:this.shiw[i].wp_lat,
        lng:this.shiw[i].wp_lng,
        name:this.shiw[i].name,
        decs:this.shiw[i].decs,
        path:this.shiw[i].path
    })
    }
  });
}

infoBusStop(){
  this.apiService.getBus_stop().subscribe((busstop:Interface_busstop[]) =>{
    this.info_data = busstop['data'];
    console.log(this.info_data);
  });
}

callAllWaypoint(){
  this.apiService.getWaypoint().subscribe(res => {
    for (let i = 0; i < res.length; i++) {
      const index1 = this.wp.map(e => e.properties.name).indexOf(res[i].properties.name)
      this.location.waypoint_marker.push({
        lat:this.wp[index1].geometry.coordinates[1],
        lng:this.wp[index1].geometry.coordinates[0],
        name:this.wp[index1].properties.name,
        decs:"",
        path:""
    })
    }    
  })
}
checkifelsebusstop(event){
    if (event.checked) {
      this.callBusStop();     
    } else {
      this.location.markers = [];
    }
}
checkifelsewaypoint(event){
  if (event.checked) {
    this.callAllWaypoint();     
  } else {
    this.location.waypoint_marker = [];
  }
}
  receiveData() {
      this.socket.connect()
        this.socket.fromEvent('sendData').subscribe(data => {
          this.flow_data = data;
          console.log("Received", data)
          let checkBus = 0;
          if (this.flow_data.bus_path == "RED"){
            this.flow_data.bus_path = "แดง";
          }
          else if (this.flow_data.bus_path == "YELLOW"){
            this.flow_data.bus_path = "เหลือง"
          }
          if (this.stream_datas.length != 0) {
            console.log('this.flow_data.device_name ', this.flow_data.device_name);
            for (let i = 0; i <= this.stream_datas.length - 1; i++) {
              if (this.flow_data.device_name == this.stream_datas[i].device_name) {
                checkBus = 1;
                this.stream_datas[i] = data;
                break;
              }
            }
          }
          if (checkBus == 0) {
            this.stream_datas.push(this.flow_data)
            this.number_bus += 1;
          }
          console.log('list : ', this.stream_datas)
          this.updateMap(this.stream_datas);
        })   
    }
  updateMap(locations) {
    console.log(this.location.bus_marker);
    this.location.bus_marker = [];
    for (let loc of locations) {
      this.location.bus_marker.push({ 
        lat:loc.wp_lat,
        lng:loc.wp_lng,
        name:loc.bus,
        path:loc.bus_path,
      })
    } 
  }
}


interface Marker {
  lat: number;
  lng: number;
  name: string;
  decs: string;
  path: string;
}
interface Name{
  lat: number;
  lng: number;
  name: string;
  path: string;
}
interface Location {
  latitude: number;
  longitude: number;
  zoom: number;
  scrollwheel:boolean;
  disableDoubleClickZoom: boolean;
  markers: Array<Marker>;
  iconUrl: string;
  bus_marker: Array<Name>;
  waypoint_marker: Array<Marker>;
  name: Array<Name>;
  path: string;

}


