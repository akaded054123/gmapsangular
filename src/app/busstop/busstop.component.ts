import { Component, OnInit,Inject } from '@angular/core';
import { Interface_busstop } from './interface_busstop';
import { ApiService } from 'src/services/api.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-busstop',
  templateUrl: './busstop.component.html',
  styleUrls: ['./busstop.component.scss']
})
export class BusstopComponent{
  result: string = '';
  shiw: Interface_busstop[];
  selectedValue: string;
  selectedColor: string;
  Iselected: any;
  public getPath: string;
  constructor(private apiService: ApiService, private http: HttpClient) { 
    this.getBus_Stop();
  }
  
  AddBus_Stop() {
    var name = (<HTMLInputElement>document.getElementById('name')).value;
    var lat = (<HTMLInputElement>document.getElementById('lat')).value;
    var lng = (<HTMLInputElement>document.getElementById('lng')).value;
    var path = (<HTMLInputElement>document.getElementById('path')).value;
    var decs = (<HTMLInputElement>document.getElementById('decs')).value;
    console.log(name,path, lat, lng,decs);
    this.http.post('http://localhost:3000/api/bus_stop/', {
      'name': name,
      'path':path,
      'lat': lat,
      'lng': lng,
      'decs': decs
    }).subscribe(res => {
      console.log(res);
      console.log('Success', 'Ok')
      window.alert('เพิ่มข้อมูลสำเร็จ !')
      window.location.reload();
    }, err => {
      console.log(err);
      console.log('Error', 'Error')
      window.alert('มีบางอย่างผิดพลาด !')
    });
  }
  UpdateBus_Stop() {
    var name = (<HTMLInputElement>document.getElementById('name')).value;
    var lat = (<HTMLInputElement>document.getElementById('lat')).value;
    var lng = (<HTMLInputElement>document.getElementById('lng')).value;
    var path = (<HTMLInputElement>document.getElementById('path')).value;
    var decs = (<HTMLInputElement>document.getElementById('decs')).value;
    console.log(name,path, lat, lng,decs);
    if (path == "null" || path == "undefined" || path){
      var path1 = this.getPath;
      this.http.put('http://localhost:3000/api/bus_stop/'+ this.selectedValue, {
      'name': name,
      'path':path1,
      'lat': lat,
      'lng': lng,
      'decs': decs
    }).subscribe(res => {
      console.log(res);
      console.log('Success', 'Ok')
      window.alert('แก้ไขข้อมูลสำเร็จ !')
      window.location.reload();
    }, err => {
      console.log(err);
      console.log('Error', 'Error')
      window.alert('มีบางอย่างผิดพลาด !')
    });
    }
  }
  clickSelect() {
    this.apiService.getBus_stopById(this.selectedValue).subscribe((busstop: any) => {
      this.Iselected = busstop;
      this.getPath = busstop.path;
      console.log(this.Iselected);
    });
  }
  getBus_Stop(){
    this.apiService.getBus_stop().subscribe((busstop:Interface_busstop[]) =>{
      this.shiw = busstop['data'];
      console.log(this.shiw);
    });
  }
  DeleBus_Stop() {
    var delete_busstop = window.confirm('คุณแน่ใจว่าต้องการลบข้อมูลนี้ ?');
    if (delete_busstop) {
      this.apiService.deleteBus_stop(this.selectedValue).subscribe(() => {
        window.location.reload();
        console.log("Deleted");
      });
    }else{
      window.location.reload();
    }
  }
  ClearInfo() {
    (<HTMLInputElement>document.getElementById('name')).value = '';
    (<HTMLInputElement>document.getElementById('lat')).value = '';
    (<HTMLInputElement>document.getElementById('lng')).value = '';
    (<HTMLInputElement>document.getElementById('decs')).value = '';
  
  }
  Paths: Path[] = [
    {value: 'แดง', viewValue: 'สีแดง'},
    {value: 'เหลือง', viewValue: 'สีเหลือง'},
    {value: 'แดง , เหลือง', viewValue: 'สีแดงและสีเหลือง'}
  ];
  ////////////////////////////////////////////////////////////TEST///////////////////////////////////////////
}
interface Path {
  value: string;
  viewValue: string;
}
