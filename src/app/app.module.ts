import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { MapsComponent } from './maps/maps.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from '../services/api.service';
import { ArMapsComponent } from './ar-maps/ar-maps.component';
import { BusstopComponent } from './busstop/busstop.component';
import { ShowmapsComponent } from './showmaps/showmaps.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule , MatFormFieldControl } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule , MatDatepickerModule ,MatNativeDateModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatIconModule } from '@angular/material/icon';
const config: SocketIoConfig = { url: '////////url websocket socket.io////////////', options: {} };
@NgModule({
  declarations: [
    AppComponent,
    MapsComponent,
    ArMapsComponent,
    ShowmapsComponent,
    BusstopComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: '//////////keyapi google map////////////',
      libraries: ["places", "geometry"]
      /* apiKey is required, unless you are a premium customer, in which case you can use clientId */
  }),
    BrowserAnimationsModule,
    MatCardModule,
    MatTabsModule,
    MatToolbarModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
    MatBottomSheetModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatIconModule,
    NgxMatSelectSearchModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [ApiService,MatDatepickerModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
