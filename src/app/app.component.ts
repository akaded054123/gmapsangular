import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  result: string = '';
  title = 'NUBUS';
  navLinks = [
    {path: 'maps', label: 'แผนที่'},
    //{path: 'history' ,label: 'History'},
    {path: 'setting_businfo' ,label: 'ระบบจัดการรถไฟฟ้า'},
    {path: 'setting_busstop',label: 'ระบบจัดการป้ายรถไฟฟ้า' }
  ];
  constructor(){}
  
}
