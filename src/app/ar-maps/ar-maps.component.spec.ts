import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArMapsComponent } from './ar-maps.component';

describe('ArMapsComponent', () => {
  let component: ArMapsComponent;
  let fixture: ComponentFixture<ArMapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArMapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
