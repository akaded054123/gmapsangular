import { Component, OnInit,Inject } from '@angular/core';
import { Interface_armaps } from './interface_armaps';
import { ApiService } from 'src/services/api.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

interface Color {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-ar-maps',
  templateUrl: './ar-maps.component.html',
  styleUrls: ['./ar-maps.component.scss']
})
export class ArMapsComponent{
  result: string = '';
  shiw: Interface_armaps[];
  selectedValue: string;
  selectedColor: string;
  Iselected: any;
  public getPath: string;
  constructor(private apiService: ApiService, private http: HttpClient) { 
    this.getBusInfo();
  }
  colors: Color[] = [
    {value: 'RED', viewValue: 'สีแดง'},
    {value: 'YELLOW', viewValue: 'สีเหลือง'},
  ];
  AddBusInfo() {
    var device_name = (<HTMLInputElement>document.getElementById('device_name')).value;
    var name = (<HTMLInputElement>document.getElementById('name')).value;
    var path = (<HTMLInputElement>document.getElementById('path')).value;
    console.log(device_name, name, path,"");
    this.http.post('http://localhost:3000/api/bus_info/', {
      'device_name': device_name,
      'name': name,
      'path': path,
      'decs': ""
    }).subscribe(res => {
      console.log(res);
      console.log('Success', 'Ok')
      window.alert('เพิ่มข้อมูลสำเร็จ !')
      window.location.reload();
    }, err => {
      console.log(err);
      console.log('Error', 'Error')
      window.alert('มีบางอย่างผิดพลาด !')
    });
  }
  UpdateBusInfo() {
    var device_name = (<HTMLInputElement>document.getElementById('device_name')).value;
    var name = (<HTMLInputElement>document.getElementById('name')).value;
    var path = (<HTMLInputElement>document.getElementById('path')).value;
    console.log(device_name, name, path,"");
    if (path == "null" || path == "undefined" || path){
      var path1 = this.getPath;
      this.http.put('http://localhost:3000/api/bus_info/'+ this.selectedValue, {
      'device_name': device_name,
      'name': name,
      'path': path1,
      'decs': ""
    }).subscribe(res => {
      console.log(res);
      console.log('Success', 'Ok')
      window.alert('แก้ไขข้อมูลสำเร็จ !')
      window.location.reload();
    }, err => {
      console.log(err);
      console.log('Error', 'Error')
      window.alert('มีบางอย่างผิดพลาด !')
    });
  }
  }
  clickSelect() {
    this.apiService.getBus_infoById(this.selectedValue).subscribe((businfo: any) => {
      this.Iselected = businfo;
      this.getPath = businfo.path;
      console.log(this.Iselected);
    });
  }
  getBusInfo(){
    this.apiService.getBus_info().subscribe((businfo:Interface_armaps[]) =>{
      this.shiw = businfo['data'];
      console.log(this.shiw);
    });
  }
  DeleBusInfo() {
    var delete_info = window.confirm('คุณแน่ใจว่าต้องการลบข้อมูลนี้ ?');
    if (delete_info) {
      this.apiService.deleteBus_info(this.selectedValue).subscribe(() => {
        window.location.reload();
        console.log("Deleted");
      });
    }else{
      window.location.reload();
    }
 
  }
  ClearInfo() {
    (<HTMLInputElement>document.getElementById('name')).value = '';
    (<HTMLInputElement>document.getElementById('device_name')).value = '';
  }
}
