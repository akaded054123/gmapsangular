import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowmapsComponent } from './showmaps.component';

describe('ShowmapsComponent', () => {
  let component: ShowmapsComponent;
  let fixture: ComponentFixture<ShowmapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowmapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowmapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
