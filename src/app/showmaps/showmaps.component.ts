import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/services/api.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-showmaps',
  templateUrl: './showmaps.component.html',
  styleUrls: ['./showmaps.component.scss']
})
export class ShowmapsComponent implements OnInit {
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  lat:any;
  lng:any;
  data: any;
  location: Location;
  wp: any;
  constructor(public apiService: ApiService) { }

  ngOnInit() {
    this.location = {
      latitude: 16.744822,
      longitude: 100.194718,
      zoom: 16,
      scrollwheel: false,
      disableDoubleClickZoom: true,
      markers: [
        {
          lat: -28.68352,
          lng: -147.20785
        }
      ]
    }
  }
}
interface Marker {
  lat: number;
  lng: number;
}
interface Location {
  latitude: number;
  longitude: number;
  zoom: number;
  scrollwheel: boolean;
  disableDoubleClickZoom: boolean;
  markers: Array<Marker>;
}
