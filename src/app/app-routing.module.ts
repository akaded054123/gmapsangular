import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapsComponent } from './maps/maps.component';
import { ArMapsComponent } from './ar-maps/ar-maps.component';
import { ShowmapsComponent } from './showmaps/showmaps.component';
import { BusstopComponent } from './busstop/busstop.component';
const appRoutes: Routes = [
  {path : '' , component : MapsComponent},
  {path : 'maps' , component : MapsComponent},
  {path : 'setting_businfo', component : ArMapsComponent},
  {path : 'history', component : ShowmapsComponent},
  {path : 'setting_busstop', component: BusstopComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes,{
    enableTracing: true
  })],
  
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }
